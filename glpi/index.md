---
layout: default
title: GLPI Nightly Builds
---

Version|Archive|Build date|Size
---|---|---|---
main|[main-f01ae24.tar.gz](main-f01ae24.tar.gz)|2025-03-06 00:34:26 UTC|81517550
10.0|[10.0-934ad3b.tar.gz](10.0-934ad3b.tar.gz)|2025-03-05 00:33:22 UTC|59739395

<font size="1">Page generated on 2025-03-06 00:34:26 UTC</font>
